<!-- Page Footer Section. Begins -->
<div class="footer section-space100">
    <!-- footer -->
    <div class="container ">
        <div class="row ">
            <div class="col-xl-4 col-lg-4 d-md-none col-sm-12 col-12  ">
                <div class="footer-logo ">
                    <!-- Footer Logo -->
                    <img src="images/new-car%20logo.png" alt="Borrow - Loan Company Website Templates ">
                </div>
                <!-- /.Footer Logo -->
            </div>
        </div>
        <hr class="dark-line ">
        <div class="row ">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <div class="widget-text mt40 ">
                    <!-- widget text -->
                    <p>CARSTORES is a leading branded auto finance sector organisation having its head office
                        situated in Chennai. We are preeminent in financial analysis on auto loan division in the
                            moto to achieve our customer and client satisfaction. We mainly accentuate car loans
                            with a business of a monthly turnover of 2.5 crores and yearly 50 crores with no
                            delinquency over all our years of services..</p>
                    <div class="row ">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                            <p class="address-text "><span><i class="icon-placeholder-3 icon-1x "></i> </span>2171, 1st floor, front house,
                            Anna nagar west, Chennai-600040 </p>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                            <p class="call-text "><span><i class="icon-phone-call icon-1x "></i></span>1800 258 5550</p>
                        </div>
                    </div>
                </div>
                <!-- /.widget text -->
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 ">
                <div class="widget-footer mt40 ">
                    <!-- widget footer -->
                    <ul class="listnone ">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="faq.html">Faq</a></li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                    </ul>
                </div>
                <!-- /.widget footer -->
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 ">
                <div class="widget-footer mt40 ">
                    <!-- widget footer -->
                    <ul class="listnone ">
                        <li><a href="index-4-students-loan.html">Car Loan</a></li>
                        <li><a href="index-5-business-loan.html">Used car Loan</a></li>
                        <li><a href="refinancing-landing-page.html">Refinance</a></li>
                        <li><a href="index-6.html">Top Up</a></li>
                        <li><a href="index-compare-shop.html">Insurance</a></li>
                    </ul>
                </div>
                <!-- /.widget footer -->
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 ">
              <div class="widget-social mt40 ">
                        <!-- widget footer -->
                        <ul class="listnone ">
                            <li><a href="https://www.facebook.com/carstores.online"><i class="fab fa-facebook-f "></i>Facebook</a></li>
                            <li><a href="#!"><i class="fab fa-google-plus "></i>Google Plus</a></li>
                            <li><a href="#!"><i class="fab fa-twitter "></i>Twitter</a></li>
                            <li><a href="#!"><i class="fab fa-linkedin "></i>Linked In</a></li>
                        </ul>
                    </div>
                <!-- /.widget footer -->
            </div>
        </div>
    </div>
</div>
<div class="tiny-footer">
        <!-- tiny footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                    <p></p>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-right">
                    <p>Terms of use | Privacy Policy</p>
                </div>
            </div>
        </div>
    </div>
    <!-- back to top icon -->
    <a href="#0" class="cd-top" title="Go to top">Top</a>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../assets/js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- slider script -->
    <script src="../assets/js/owl.carousel.min.js"></script>    
    <script src="../assets/js/main.js"></script>
    <script src="../assets/js/form-wizard.js"></script>
    <!-- Back to top script -->
    <script src="../assets/js/back-to-top.js"></script>
<!-- Page Footer Section. Ends -->