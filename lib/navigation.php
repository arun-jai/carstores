<!-- Navigation Menu Section. Begins -->
<div class="">
        <div class="collapse searchbar" id="searchbar">
            <div class="search-area bg-white">
                <div class="container">
                    <div class="row">
                        <div class="offset-lg-3 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-secondary" type="button">Go!</button>
                                </span>
                                <div class="btn-close">
                                    <a class="close " aria-label="Close" data-toggle="collapse" href="#searchbar" aria-expanded="false">
                                        <span aria-hidden="true" class="fas fa-times"></span></a></div>
                            </div>
                            <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                </div>
            </div>
        </div>
        <div class="header-topbar">
            <!-- top-bar -->
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-4 d-none d-xl-block d-lg-block  cs-fz-12" >
                        <p class="mail-text cs-theme-font-color"> Welcome to carstores</p>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-4 d-none d-xl-block d-lg-block c-center">
                       <a href="credit-score.php"><img src="assets\images/ezgif.com-resize (1).gif"alt=""> <p class="cs-theme-font-color">Credit score</p></a> 
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-4 text-right  cs-fz-12">
                        <div class="top-nav "> <span class="top-text"><a href="loan-calculator.php" class=" cs-theme-font-color">EMI calculator</a> </span><span class="top-text cs-theme-font-color"><a href="credit-score.php" class=" cs-theme-font-color">Credit Score </a> </span> <span class="top-text"><a href="tel:1800258550" target="_blank" class=" cs-theme-font-color">1800 258 5550</a></span>   </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.top-bar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-lightgreen py-3">
            <div class="container">
                <a href="index.php" class="navbar-brand"><img  class="img-fluid" src="assets\images/RGB copyonly cirlce.png" alt="Carstores-The financial Market" > <b>Carstores</b><span class="sub-title">TM </span></a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar top-bar mt-0"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-white" id="navbarHome" data-toggle="dropdown" href="#!" aria-haspopup="true" aria-expanded="false">
                                Home
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg" aria-labelledby="navbarHome">
                                 <div class="row no-gutters">
                                        <div class="col-6 col-lg-6">
                                            <a class="dropdown-item mb-lg-0" href="index-creditscore.php">Credit Score<span class="badge badge-outline border-default text-default ml-2">New</span>
                                            </a>
                                            <a class="dropdown-item  mb-lg-0" href="index-compare-shop.php">Insurance<span class="badge badge-outline border-default text-default ml-2">New</span>
                                            </a>
                                        </div>
                                    </div>
                            </div> <!-- / .row -->
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-white" id="navbarProduct" data-toggle="dropdown" href="#!" aria-haspopup="true" aria-expanded="false">
                                Product
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarProduct">
                              <li class="dropdown-item dropright">
                                        <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#!">
                                            Loans
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="index-4-students-loan.php">
                                              New Car Loan
                                            </a>
                                            <a class="dropdown-item" href="index-5-business-loan.php">
                                               Used Car Loan
                                            </a>                                           
                                        </div>
                                    </li>
                                    <li class="dropdown-item dropright">
                                        <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#!">
                                            Lenders
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="lender-single.php" target="_blank">
                                                Axis BANK
                                            </a>
                                            <a class="dropdown-item" href="blog-two-column.php" target="_blank">
                                                ICICI BANK
                                            </a>
                                            <a class="dropdown-item" href="borrow-account-saving.php" target="_blank">
                                                KOTAK BANK
                                            </a>
                                            <a class="dropdown-item" href="blog-three-column.php" target="_blank">
                                                INDUSIND BANK
                                            </a>
                                            <a class="dropdown-item" href="blog-listing.php" target="_blank">
                                                BAJAJ FINSERV
                                            </a>
                                            <a class="dropdown-item" href="blog-single.php" target="_blank">
                                                EQUITAS SMALL FINANCE BANK
                                            </a>
                                            <a class="dropdown-item" href="card-landing-page.php" target="_blank">
                                                HDFC BANK
                                            </a>
                                        </div>
                                    </li>
                                    <li class="dropdown-item dropright">
                                    </li>
                                    <li class="dropdown-item dropright">
                                        <a class="dropdown-link dropdown-toggle text-info" data-toggle="dropdown" href="#!">
                                            Compare Page
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="compare-student-loan.php">
                                                Compare Car Loan
                                            </a>
                                            <a class="dropdown-item" href="compare-loan.php">
                                                Compare Used Car Loan
                                            </a>
                                        </div>
                                    </li>
                                    <li><a href="loan-calculator.php" class="dropdown-item">
                                        Loan Calculator
                                    </a>
                                    </li>
                                    <li>
                                    <a href="loan-eligibility.php" class="dropdown-item">
                                        Eligibility Calculator
                                    </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item  mb-lg-0" href="refinancing-landing-page.php">
                                            Refinance
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item  mb-lg-0" href="index-6.php">
                                            TOP UP
                                        </a>
                                    </li>
                            </ul>
                        </li>
               <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-white" href="#!" id="navbarFeatures" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Features
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarFeatures">
                                    <li><a class="dropdown-item" href="about.php">About Us</a></li>
<!--                                    <li> <a class="dropdown-item" href="team.php">Team</a></li>-->
                                    <li> <a class="dropdown-item" href="faq.php">FAQ</a></li>
<!--                                    <li> <a class="dropdown-item" href="testimonial.php">Testimonial</a></li>-->
                                    <li class="dropdown-item dropleft" >
                                        <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#!" aria-expanded="false">
                                            Gallery
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="gallery-filter-3.php">
                                                Three Column
                                            </a>
                                        </div>
                                    </li>
                                    <li> <a class="dropdown-item" href="error.php"></a></li>
                                </ul> 
                        </li>
                        <li class="nav-item">
                             <a class="nav-link text-white" href="contact-us.php">
                                    Contact us
                                </a>
                        </li>
                    </ul>
                    <div class="search-nav ml-lg-3 ml-0"> <a class="search-btn" role="button" data-toggle="collapse" href="#searchbar" aria-expanded="false"><i class="fa fa-search"></i></a> </div>
                </div>
            </div>
        </nav>
    </div>
<!-- Navigation Menu Section. Ends -->
