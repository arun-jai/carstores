<!-- ************************************************************************** -->
<!-- Page Base Requirements are Loaded. Begins -->
<!-- ************************************************************************** -->
<?php
    require_once(__DIR__ .'/lib/config.php');
    if (!isset($template)) {
        $template = new Template();
        $template->bodyContent = __FILE__;
        include "lib/template.php";
        exit;
    }
?>
<!-- ************************************************************************** -->
<!-- Page Base Requirements are Loaded. Ends -->
<!-- ************************************************************************** -->

<!-- ************************************************************************** -->
<!-- Page Content Section. Begins -->
<!-- ************************************************************************** -->
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Contact us</li>
                        </ol>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="bg-white pinside30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                                <h1 class="page-title">Contact Us</h1>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
<!--                                <div class="btn-action"> <a href="#!" class="btn btn-secondary">How To Apply</a> </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="sub-nav" id="sub-nav">
                        <ul class="nav nav-justified">
                            <li class="nav-item">
                                <a href="contact-us.html" class="nav-link">Give me call back</a>
                            </li>
                            <li class="nav-item">
                                <a href="loan-calculator.html" class="nav-link">Emi Caculator</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="wrapper-content bg-white p-3 p-lg-5">
                        <div class="contact-form mb60">
                            <div class=" ">
                                <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                                    <div class="mb60  section-title text-center  ">
                                        <!-- section title start-->
                                        <h1>Get In Touch</h1>
                                        <p>Reach out to us &amp; we will respond as soon as we can.</p>
                                    </div>
                                </div>
                                <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
    </div>
</div>
<form role="form">
    <div class="row setup-content" id="step-1">
                <h3> Step 1</h3>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">First Name<span class=" "> </span></label>
                        <input id="name" name="name" type="text" placeholder="First Name" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Last Name<span class=" "> </span></label>
                        <input id="name" name="name" type="text" placeholder="Last Name" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Date of Birth<span class=" "> </span></label>
                        <input id="name" name="name" type="date" placeholder="Date of Birth" class="form-control input-md" required>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="control-label">First Name</label>
                    <input  maxlength="100" type="text" required="required" class="form-control input-md" placeholder="Enter First Name"  />
                </div> -->
                <!-- <div class="form-group">
                    <label class="control-label">Last Name</label>
                    <input maxlength="100" type="text" required="required" class="form-control input-md" placeholder="Enter Last Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Date of Birth</label>
                    <input type="date" required="required" class="form-control input-md" placeholder="Enter Date of Birth" />
                </div> -->
                <button class="btn btn-primary nextBtn" type="button" >Next</button>
    </div>
    <div class="row setup-content" id="step-2">
                <h3> Step 2</h3>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Mobile Number<span class=" "> </span></label>
                        <input id="name" name="name" type="text" placeholder="Mobile Number" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Email<span class=" "> </span></label>
                        <input id="name" name="name" type="text" placeholder="Email" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="control-label" for="message"> </label>
                        <textarea class="form-control" id="message" rows="7" name="message" placeholder="Address"></textarea>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="control-label">First Name</label>
                    <input  maxlength="100" type="text" required="required" class="form-control input-md" placeholder="Enter First Name"  />
                </div> -->
                <!-- <div class="form-group">
                    <label class="control-label">Last Name</label>
                    <input maxlength="100" type="text" required="required" class="form-control input-md" placeholder="Enter Last Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Date of Birth</label>
                    <input type="date" required="required" class="form-control input-md" placeholder="Enter Date of Birth" />
                </div> -->
                <button class="btn btn-primary nextBtn" type="button" >Next</button>
    </div>
    <div class="row setup-content" id="step-3">
                <h3> Step 3</h3>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Aadhar Card Number<span class=" "> </span></label>
                        <input id="name" name="name" type="text" placeholder="Aadhar card Number" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Aadhar Card Front View<span class=" "> </span></label>
                        <input id="file" name="name" type="file" placeholder="Aadhar card front view" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Aadhar Card Back View<span class=" "> </span></label>
                        <input id="name" name="name" type="file" placeholder="Aadhar Card Back View" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Pan Card Number<span class=" "> </span></label>
                        <input id="name" name="name" type="text" placeholder="Pan Card Number" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Pan Card Front View<span class=" "> </span></label>
                        <input id="file" name="name" type="file" placeholder="Pan Card front view" class="form-control input-md" required>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <label class="sr-only control-label" for="name">Pan Card Back View<span class=" "> </span></label>
                        <input id="name" name="name" type="file" placeholder="Pan Card Back View" class="form-control input-md" required>
                    </div>
                </div>
                <button class="btn btn-success" type="submit">Finish!</button>
    </div>
</form>
                            </div>
                        </div>
                        <!-- /.section title start-->
                        <div class="contact-us mb60">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="mb60  section-title">
                                        <!-- section title start-->
                                        <h1>We are here to help you </h1>
                                        <p class="lead">Various versions have evolved over the years sometimes by accident sometimes on purpose injected humour and the like.</p>
                                    </div>
                                    <!-- /.section title start-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div class="bg-boxshadow pinside60 outline text-center mb30">
                                        <div class="mb40"><i class="icon-briefcase icon-2x icon-default"></i></div>
                                        <h2 class="capital-title">Branch Office</h2>
                                        <p>2171,1st floor,front house, Anna nagar,West Chennai-600040
                                            <br> West Chennai-600040</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div class="bg-boxshadow pinside60 outline text-center mb30">
                                        <div class="mb40"><i class="icon-phone-call icon-2x icon-default"></i></div>
                                        <a href="tel:1800258550"> <h2 class="capital-title">Call us at </h2></a>
                                        <h1 class="text-big">1800 258 5550 </h1>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div class="bg-boxshadow pinside60 outline text-center mb30">
                                        <div class="mb40"> <i class="icon-letter icon-2x icon-default"></i></div>
                                        <a href="mailto:mathew@carstores.online?subject=Enquiry" ><h2 class="capital-title">Email Address</h2></a>
                                        <p>mathew@carstores.online</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="map center" id="">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d7865258.138012562!2d72.13788981665967!3d15.72688840195785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d18.989055999999998!2d73.1316224!4m5!1s0x3a526556f7b3abc7%3A0x73c2ac81262a7614!2scarstores%20chennai!3m2!1d13.083582999999999!2d80.2023214!5e0!3m2!1sen!2sin!4v1599301209789!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
    <!-- ************************************************************************** -->
<!-- Page Content Section. Ends -->
<!-- ************************************************************************** -->