<!-- ************************************************************************** -->
<!-- Page Base Requirements are Loaded. Begins -->
<!-- ************************************************************************** -->
<?php
    require_once(__DIR__ .'/lib/config.php');
    if (!isset($template)) {
        $template = new Template();
        $template->bodyContent = __FILE__;
        include "lib/template.php";
        exit;
    }
?>
<!-- ************************************************************************** -->
<!-- Page Base Requirements are Loaded. Ends -->
<!-- ************************************************************************** -->

<!-- ************************************************************************** -->
<!-- Page Content Section. Begins -->
<!-- ************************************************************************** -->
<div class="slider" id="slider">
        <!-- slider -->
        <div class="slider-img"><img src="assets/images/Carstsxdsdore.jpg" alt="Borrow - Loan Company Website Template" class="">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="slider-captions">
                                <!-- slider-captions -->
                                <h1 class="slider-title"> Lowest Car Loan Rate  </h1>
                                <p class="slider-text d-none d-xl-block d-lg-block d-sm-block"> We provide an excellent service Loan company.</p>
                                <a href="loan-eligibility.html" class="btn btn-danger">Check Eligiblity</a>
                            </div>
                            <!-- /.slider-captions -->
                        </div>
                    </div>
                </div>
            </div>
        
        <div>
           <div class="slider-img"><img src="assets/images/Carstore2.jpg" alt="Borrow - Loan Company Website Template" class="">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="slider-captions">
                                <!-- slider-captions -->
                                <h1 class="slider-title">Used Car Loans with Great Rates </h1>
                                <p class="slider-text d-none d-xl-block d-lg-block d-sm-block">We provide an excellent service for all types of loans
                                <a href="loan-listing-image.html" class="btn btn-danger">View Products</a>
                            </div>
                            <!-- /.slider-captions -->
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div>
            <div class="slider-img"><img src="assets\images\first.jpg" alt="Borrow - Loan Company Website Template" class="">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="slider-captions">
                            <!-- slider-captions -->
                            <h1 class="slider-title" >Car Loan to Suit Your Needs. </h1>
                            <p class="slider-text d-none d-xl-block d-lg-block d-sm-block">The low rate you need for the need you want! Call
                                <br>
                                <strong class="text-highlight text-olive">1800-258-5550</strong></p>
                            <a href="loan-listing-image.html" class="btn btn-danger">Loan Products</a>
                        </div>
                        <!-- /.slider-captions -->
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="rate-table">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="rate-counter-block">
                        <div class="icon rate-icon  "> <img src="assets\images/car.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate">3.74%</h1>
                            <small class="rate-title">Insurance</small>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="rate-counter-block">
                        <div class="icon rate-icon "> <img src="assets\images/loan.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate">8.96%</h1>
                            <small class="rate-title">Used Car Loans</small>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="rate-counter-block">
                        <div class="icon rate-icon "> <img src="assets\images/car.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate">6.70%</h1>
                            <small class="rate-title">Car Loans</small>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="rate-counter-block">
                        <div class="icon rate-icon  "> <img src="assets\images/credit-card.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate">9.00%</h1>
                            <small class="rate-title">Balance Top-Up</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb60 text-center section-title">
                        <!-- section title start-->
                        <h1 class="font">Find Loan Products We Offers</h1>
                        <p>We will match you with a loan program that meet your financial need. In short term liquidity, by striving to make funds available to them <strong>within 24 hours of application.</strong></p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="service" id="service">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="assets\images/loan.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-2x"> </div>
                            <h2><a href="index-4-students-loan.html" class="title">Car Loan</a></h2>
<!--                            <p>Lorem ipsum dolor sit ameectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>-->
                            <a href="index-4-students-loan.html" class="btn-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="assets\images/user-tie.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-2x"></div>
                            <h2><a href="index-5-business-loan.html" class="title">Used Car Loan</a></h2>
<!--                            <p>Sed ut perspiciatis unde omnis rror sit voluptatem accusan tium dolo remque laudantium, totam rem aperiam, eaque ipsa </p>-->
                            <a href="index-5-business-loan.html" class="btn-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="assets\images/piggy-bank.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-2x"></div>
                            <h2><a href="index-6.html" class="title">Balance Top-Up</a></h2>
<!--                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elmodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>-->
                            <a href="index-6.html" class="btn-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="assets\images/growth.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-2x"></div>
                            <h2><a href="index-creditscore.html" class="title">Credit Score</a></h2>
<!--                            <p>Lorem ipsum dolor sit nean commodo ligula eget dolor simple dummyum sociis natoque.amet, consectetuer adipiscing elit. </p>-->
                            <a href="index-creditscore.html" class="btn-link">Read More</a>
                        </div>
                    </div>
                    
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="assets\images/car.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-2x"></div>
                            <h2><a href="loan-calculator.html" class="title">EMI Calculator</a></h2>
<!--                            <p>Lorem ipsum dolor sit nean commodo ligula eget dolor simple dummyum sociis natoque.amet, consectetuer adipiscing elit. </p>-->
                            <a href="loan-calculator.html" class="btn-link">Read More</a>
                        </div>
                    </div>
<!--
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="bg-white pinside40 service-block outline mb30">
                            <div class="icon mb40"> <img src="assets\images/car.svg" alt="Borrow - Loan Company Website Template" class="icon-svg-2x"></div>
                            <h2><a href="#!" class="title">Car Loan</a></h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque.</p>
                            <a href="#!" class="btn-link">Read More</a>
                        </div>
                    </div>
-->
                </div>
            </div>
        </div>
    </div>
    <div class="section-space80 bg-white call-to-action">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-md-10 col-sm-12 col-12">
                        <div class="mt40">
                            <!-- section title start-->
                            <h1 class="text-darkgreen">Get Your Credit Score - It's FREE</h1>
                            <p class="text-dark">One powerful number that puts you in control! </p>
                            <h3 class="text-darkgreen">Just a step away</h3>
                            
                                      <a href="index-creditscore.html">  <button type="submit" class="btn btn-color btn-block">Credit Score</button></a>
                                    
                        </div>
                        <!-- /.section title start-->
                    </div>
                    <div class="offset-xl-2 col-lg-5 offset-lg-2 col-lg-5 col-md-6 col-sm-12 col-12">
                        <div class="mt20">
                            <a href="index-creditscore.html"><img src="assets\images/giphy.gif" class="img-fluid"></a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="bg-white section-space80">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb100 text-center section-title">
                        <!-- section title start-->
                        <h1>Fast &amp; Easy Application Process.</h1>
                        <p>We speed your financial wheels</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div class="circle"><span class="number">1</span></div>
                        <h3 class="number-title mb-1">Choose Loan Amount</h3>
                        <p>Processing a car loan is very easy and you can get one in a few simple steps.<br> </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div class="circle"><span class="number">2</span></div>
                        <h3 class="number-title mb-1">Approved Your Loan</h3>
                        <p>EMI calculators & Credit score check available on our websites to decide on your loan amount and tenure</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="bg-white pinside40 number-block outline mb60 bg-boxshadow">
                        <div class="circle"><span class="number">3</span></div>
                        <h3 class="number-title mb-1">Get Your Car</h3>
                        <p>Carstores approve your loan instantly, according to the procedures.<br></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12 text-center"> <a href="loan-listing-image.html" class="btn btn-danger">View Our Loans</a> </div>
            </div>
        </div>
    </div>
    <div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb60 text-center section-title">
                        <!-- section title start-->
                        <h1>Why People Choose Us</h1>
                        <p>There's more than one reason to sign up with confidence</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="bg-white bg-boxshadow">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 nopadding">
                                <div class="bg-white pinside60 number-block outline">
                                    <div class="mb20"><i class="icon-command  icon-4x icon-primary"></i></div>
                                    <h3>Dedicated Specialists</h3>
                                    <p>We are the specialists in public relations, other scenarios involving the legal, ethical, or financial standing of the entity to the customers.We offer diverse needs,the team of financial experts will offer the finest service levels, end-to-end solutions to your current and future needs.<br><br></p>
<!--                                    <a href="team.html" class="btn btn-outline-default mt20">Meet the team</a>-->
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 nopadding">
                                <div class="bg-white pinside60 number-block outline">
                                    <div class="mb20"><i class="icon-cup  icon-4x icon-primary"></i></div>
                                    <h3>Success Stories Rating</h3>
                                    <p>A quick read..<br>car stores are one of the best researchers and facilitated loans for the auto industry with the necessary services for customers. We promise 100% satisfaction with all the customer's needs based on their criteria. Look what are customers have to say<br><br></p>
<!--                                    <a href="testimonial.html" class="btn btn-outline-default mt20">View Client Review</a>-->
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 nopadding">
                                <div class="bg-white pinside60 number-block outline">
                                    <div class="mb20"><i class="icon-calculator  icon-4x icon-primary"></i></div>
                                    <h3>Carstores venture!</h3>
                                    <p>Carstores is a Chennai based company besides, there been a successful financial organization in the industry from 4 years for auto loans. We have reached a milestone of 50 crores .since, started from 5 crores a year. By focusing on customer satisfaction. Our primary key is to complete RC work in 30days.</p>
<!--                                    <a href="about.html" class="btn btn-outline-default mt20">Why choose us</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-default section-space80">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb60 text-center section-title">
                        <!-- section title start-->
                        <h1 class="title-white">Some of our Awesome Testimonials</h1>
<!--                        <p class="text-white"> You won’t be the only one lorem ipsu mauris diam mattises.</p>-->
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-4 col-12 clearfix ">
                    <div class="testimonial-block mb30">
                        <div class="bg-white pinside30 mb20">
                            <p class="testimonial-text"> “I loved the customer service you guys provided me. That was very nice and patient with questions I had. I would really like definitely come back here”</p>
                        </div>
                        <div class="testimonial-autor-box">
                            <div class="testimonial-img pull-left"> <img src="assets\images/avatar-1.png" alt="Borrow - Loan Company Website Template"> </div>
                            <div class="testimonial-autor pull-left">
                                <h4 class="testimonial-name">Donny J. Griffin</h4>
                                <span class="testimonial-meta text-white">Insurance</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-4 col-12 clearfix ">
                    <div class="testimonial-block mb30">
                        <div class="bg-white pinside30 mb20">
                            <p class="testimonial-text"> “I had a good experience with carstores Loan Services. I am thankful to carstores for the help you guys gave me. My loan was easy and fast. thank you.”</p>
                        </div>
                        <div class="testimonial-autor-box">
                            <div class="testimonial-img pull-left"> <img src="assets\images/avatar-2.png" alt="Borrow - Loan Company Website Template"> </div>
                            <div class="testimonial-autor pull-left">
                                <h4 class="testimonial-name">Mary O. Randle</h4>
                                <span class="testimonial-meta text-white">Car Loan</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-4 col-12 clearfix ">
                    <div class="testimonial-block mb30">
                        <div class="bg-white pinside30 mb20">
                            <p class="testimonial-text"> “We came out of their offices very happy with their service. They treated us very kind. Definite will come back. The waiting time was very appropriate.”</p>
                        </div>
                        <div class="testimonial-autor-box">
                            <div class="testimonial-img pull-left"> <img src="assets\images/avatar-3.png" alt="Borrow - Loan Company Website Template"> </div>
                            <div class="testimonial-autor pull-left">
                                <h4 class="testimonial-name">Lindo E. Olson</h4>
                                <span class="testimonial-meta text-white">Used Car Loan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-space40 bgimg2">
        <div class="container">
            <div class="row" >
               
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6"> <a href="lender-single.html" target="_blank"><img src="assets\images/logos/axis Bank.jpg"  alt="Borrow - Loan Company Website Template"></a> </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6"> <a href="blog-listing.html" target="_blank"><img src="assets\images/logos/bajaj finserve.jpg" alt="Borrow - Loan Company Website Template"> </a></div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6"> <a href="blog-single.html" target="_blank"><img src="assets\images/logos/equitas bank.jpg"  alt="Borrow - Loan Company Website Template"> </a></div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6"> <a href="blog-three-column.html" target="_blank"><img src="assets\images/logos/indusland bank.jpg" alt="Borrow - Loan Company Website Template"></a> </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6"> <a href="blog-two-column.html" target="_blank"><img src="assets\images/logos/ICICI BANK.jpg" alt="Borrow - Loan Company Website Template"></a> </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-6"><a href="borrow-account-saving.html"> <img src="assets\images/logos/kotak.jpg" alt="Borrow - Loan Company Website Template"></a> </div>
            </div>
        </div>
    </div>
    <div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb60 text-center section-title">
                        <!-- section title start-->
                        <h1>Latest News from Carstores</h1>
                        <p> Our mission is to deliver reliable, latest news and opinions.</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="assets\images/selection2.jpg"  alt="Borrow - Loan Company Website Template" class="img-fluid"></a>
                        </div>
                        <div class="bg-white pinside40 outline">
                            <h2><a href="index-4-students-loan.html" class="title">New Car Loan</a></h2>
                            <p class="meta"><span class="meta-date">Aug 25, 2017</span><span class="meta-author">By<a href="#!"> Admin</a></span></p>
                            <p>New Car finance refere to the various financial products which allow customer to acquire a car.</p>
<!--                            <a href="blog-single.html" class="btn-link">Read More</a>-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="assets\images/Webp.net-resizeimage%20(3).jpg"  alt="Borrow - Loan Company Website Template" class="img-fluid"></a>
                        </div>
                        <div class="bg-white pinside40 outline">
                            <h2><a href="index-compare-shop.html" class="title">Thinking for Insurance</a></h2>
                            <p class="meta"><span class="meta-date">Aug 24, 2017</span><span class="meta-author">By<a href="#!"> Admin</a></span></p>
                            <p>Insurance is means of protection from financial loss. An entity which provides insurance is known as the financial market</p>
<!--                            <a href="blog-single.html" class="btn-link">Read More</a>-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="assets\images/Webp.net-resizeimage%20(5).jpg" alt="Borrow - Loan Company Website Templates" class="img-fluid"></a>
                        </div>
                        <div class="bg-white pinside40 outline">
                            <h2><a href="refinancing-landing-page.html" class="title">Are you looking for Refinance ?</a></h2>
                            <p class="meta"><span class="meta-date">Aug 23, 2017</span><span class="meta-author">By<a href="#!"> Admin</a></span></p>
                            <p>Refinancing involves the re-evoluation of a customer credit terms and status.</p>
<!--                            <a href="blog-single.html" class="btn-link">Read More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-space80 bg-white">
        <div class="container">
            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb60 text-center section-title">
                        <!-- section title-->
                        <h1>We are Here to Help You</h1>
                        <p>Our mission is to deliver reliable, latest news and opinions.</p>
                    </div>
                    <!-- /.section title-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"><i class="icon-calendar-3 icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Apply For Loan</h2>
                        <p>Looking to buy a car or Used car loan? then apply for loan now.</p>
                        <a href="mailto:mathew@carstores.online?subject=Enquiry" class="btn-link">Get Appointment</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"><i class="icon-phone-call icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Call us at </h2>
                         <h1 class="text-big">+1800 258 5550</h1>
                        <p>www.carstores.online</p>
                        <a href="tel:1800258550" target="_blank" class="btn-link">Contact us</a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"> <i class="icon-users icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Talk to Advisor</h2>
                        <p>Need to loan advise? Talk to our Loan advisors.</p>
                        <a href="contact-us.html" class="btn-link">Meet The Advisor</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ************************************************************************** -->
<!-- Page Content Section. Ends -->
<!-- ************************************************************************** -->