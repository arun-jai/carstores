<!-- ************************************************************************** -->
<!-- Page Base Requirements are Loaded. Begins -->
<!-- ************************************************************************** -->
<?php
    require_once(__DIR__ .'/lib/config.php');
    if (!isset($template)) {
        $template = new Template();
        $template->bodyContent = __FILE__;
        include "lib/template.php";
        exit;
    }
?>
<!-- ************************************************************************** -->
<!-- Page Base Requirements are Loaded. Ends -->
<!-- ************************************************************************** -->

<!-- ************************************************************************** -->
<!-- Page Content Section. Begins -->
<!-- ************************************************************************** -->
<div class="bg-light">
      <div class="container">
        <div class="row d-flex align-items-center">
          <div class="col-lg-5 col-md-12 col-12">
            <div class="p-3 p-lg-0">
              <h1 class="display-5">Get Your Credit Score - It's FREE</h1>
              <p>
                The smart choice when it comes to finding credit that’s just
                right for you
              </p>
              <div class="mb-4">
                <a href="credit-score.html" class="btn btn-secondary">Sign up free</a>
                <a href="#!" class="btn btn-outline-primary">Learn</a>
              </div>
              <p class="mb-0">No credit card required—ever</p>
              <p>Checking your score won't hurt your credit</p>
              <a href="#!" class="btn-link">Advertiser Disclosure</a>
            </div>
          </div>
          <div class="col-lg-7 col-md-7 col-12 d-md-none d-lg-block">
            <div class="pdt60">
              <img
                src="assets/images/hero-creditstore.png"
                alt="Borrow - Loan Company Responsive Website Templates"
                class="img-fluid"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section-space80 bg-white">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-12">
            <div class="mb-5 text-center">
              <h1>How the service works</h1>
              <p>
                Being aware of your credit score may allow you to negotiate a
                better rate <br />with some lenders.
              </p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card bg-white">
              <div class="card-body">
                <img src="assets/icon/icon-1.svg" alt="Borrow - Loan Company Responsive Website Templates" class="mb-4 icon-lg" />
                <p>
                  Fill out your profile so that we can accurately determine your
                  credit rating.
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card bg-white">
              <div class="card-body">
                <img src="assets/icon/icon-2.svg" alt="Borrow - Loan Company Responsive Website Templates" class="mb-4 icon-lg" />
                <p>
                  We got the your form request and We will make your personal
                  rating and solutions.
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card bg-white">
              <div class="card-body">
                <img src="assets/icon/icon-3.svg" alt="Borrow - Loan Company Responsive Website Templates" class="mb-4 icon-lg" />
                <p>
                  We will offer you recommendations on how to improve your
                  rating.
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card bg-white">
              <div class="card-body">
                <img src="assets/icon/icon-4.svg" alt="Borrow - Loan Company Responsive Website Templates" class="mb-4 icon-lg" />
                <p>
                  An additional bouns for cutomers with a good rating is a
                  credit solution from banks online.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
<!--      <hr class="pdt100 mt100" />-->
<!--
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-12">
            <div class="mb60 text-center">
              <h1>We’ll help you make smarter choices</h1>
              <p>
                We’ve got the data that matters. We use it to benefit you best
                by finding <br />
                credit cards and personal loans that you’re more likely to get.
              </p>
            </div>
          </div>
        </div>
        <div class="row align-items-center">
          <div class="offset-lg-1 col-lg-4 col-md-6 col-12">
            <div class="media align-items-center mb-4">
              <img
                src="assets\images/avatar-1.png"
                class="mr-3 icon-md icon-shape rounded-circle"
                alt="..."
              />
              <div class="media-body">
                <h5 class="mb-0">Hi, Bernard Fisher</h5>
              </div>
            </div>
            <h5 class="mb-3">Your credit card result</h5>
            <div class="card bg-white mb-3 shadow">
              <span class="badge badge-info badge-rate font-11 float-right"
                >Guaranteed Rate</span
              >
              <div class="card-body p-4">
                <div class="media align-items-center">
                  <img src="assets\images/card-1.png" alt="Borrow - Loan Company Responsive Website Templates" class="mr-3 w-25" />
                  <div class="media-body">
                    <h6 class="mb-1">Borrow Plus Money</h6>
                    <p class="font-10 mb-0">
                      <span class="font-14 text-dark">21.9%</span>
                      <span class="mr-1">Guaranteed APR</span>
                      <span
                        ><i class="fas fa-check-circle mr-1 text-success"></i
                        >Pre Approved</span
                      >
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card bg-white mt-2 mb-3 shadow">
              <div class="card-body p-4">
                <div class="media align-items-center">
                  <img src="assets\images/card-2.png" alt="Borrow - Loan Company Responsive Website Templates" class="mr-3 w-25" />
                  <div class="media-body">
                    <h6 class="mb-1">Borrow Demo Privileges</h6>
                    <p class="font-10 mb-0">
                      <span class="font-14 text-dark">20.9%</span>
                      <span class="mr-1">Guaranteed APR</span>
                      <span
                        ><i class="fas fa-check-circle mr-1 text-success"></i
                        >Pre Approved</span
                      >
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card bg-white shadow">
              <div class="card-body p-4">
                <div class="media align-items-center">
                  <img src="assets\images/card-3.png" alt="Borrow - Loan Company Responsive Website Templates" class="mr-3 w-25" />
                  <div class="media-body">
                    <h6 class="mb-1">Borrow Money Back</h6>
                    <p class="font-10 mb-0">
                      <span class="font-14 text-dark">18.2%</span>
                      <span class="mr-1">Guaranteed APR</span>
                      <span
                        ><img
                          src="assets\images/dougnut-small.svg"
                          alt="Borrow - Loan Company Responsive Website Templates"
                          class="mr-1 icon"
                        />
                        Eligiblity</span
                      >
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="offset-lg-1 col-lg-5 col-md-6 col-12">
            <div class="media mb-5">
              <img src="assets\images/eligibility.svg" class="mr-4" alt="..." />
              <div class="media-body">
                <h3 class="mt-0">Eligibility</h3>
                See how likely you are to be accepted for certain offers, before
                applying
              </div>
            </div>
            <div class="media mb-5">
              <img src="assets\images/preapproval.svg" class="mr-4" alt="..." />
              <div class="media-body">
                <h3 class="mt-0">Pre-approval</h3>
                Our green tick gives you the green-light to apply with
                confidence
              </div>
            </div>
            <div class="media">
              <img src="assets\images/guaranteed.svg" class="mr-4" alt="..." />
              <div class="media-body">
                <h3 class="mt-0">Guaranteed rates</h3>
                These rates are guaranteed, so the rate you see, is the rate
                you’ll get
              </div>
            </div>
          </div>
        </div>
      </div>
-->
    </div>
    <div class="section-space80 bg-olive">
      <div class="container">
        <div class="row">
          <div class="offset-lg-3 col-lg-6 col-md-12 col-12">
            <div class="text-center">
              <h1 class="text-white">Get started - it's FREE!</h1>
              <p class="text-white">
                Receive & understand your credit score and access relevant
                financial information & offers.
              </p>
              <a href="credit-score.html" class="btn btn-secondary">Get Your Score Free</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ************************************************************************** -->
<!-- Page Content Section. Ends -->
<!-- ************************************************************************** -->